import tcod as tc

from game_states import GameStates
from renderFunctions import RenderOrder
from map_objects.game_messages import Message

def kill_player(player):
    player.char = 'X'
    player.color = tc.dark_red

    return Message('You died!', tc.red), GameStates.PLAYER_DEAD

def kill_monster(monster):
    death_message = Message('{0} is dead!'.format(monster.name.capitalize()), tc.orange)

    monster.char = '%'
    monster.color = tc.dark_red
    monster.blocks = False
    monster.fighter = None
    monster.ai = None
    monster.name = 'remains of ' + monster.name.capitalize()
    monster.render_order = RenderOrder.CORPSE

    return death_message
