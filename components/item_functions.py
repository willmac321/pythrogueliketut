import tcod as tc
from random import randint
from map_objects.game_messages import Message
from character import Character

def heal(*args, **kwargs):
    entity = args[0]
    amount = kwargs.get('amount')

    results = []

    if entity.fighter.hp == entity.fighter.max_hp:
        results.append({'consumed': False, 'message': Message('You are already at full health', tc.yellow)})
    else:
        entity.fighter.heal(amount)
        results.append({'consumed': True, 'message': Message('Your wounds start to feel better!', tc.green)})

    return results

def shoot_crossbow(*args, **kwargs):
    caster = args[0]
    entities = kwargs.get('entities')
    fov_map = kwargs.get('fov_map')
    tc_map = kwargs.get('tc_map')
    damage = kwargs.get('damage')
    damage = randint(int(damage/3), damage)
    radius = kwargs.get('radius')
    target_x = kwargs.get('target_x')
    target_y = kwargs.get('target_y')
    ammunition = kwargs.get('ammunition')
    results = []

    if not tc.map_is_in_fov(fov_map, target_x, target_y):
        results.append({'consumed': False, 'message': Message('You cannot target a tile outside your field of view.', tc.yellow)})
        return results
    if not tc.map_is_in_fov(tc_map, target_x, target_y):
        results.append({'consumed': False, 'message': Message('Target is too far away!', tc.yellow)})
        return results
    for e in entities:
        if ammunition != None and ammunition > 0 and target_x == e.x and target_y == e.y and e.fighter and caster.distance_to(e) <= radius and e != caster:
            target = e
            ammunition = int(ammunition) - 1
            results.append({'consumed': False, 'item_used' : True, 'ammunition' : ammunition, 'target': target, 'message': Message('A crossbow bolt strikes the {0}! The damage is {1}'.format(target.name, damage))})
            results.extend(target.fighter.take_damage(damage))
        elif ammunition != None and ammunition < 1:
            results.append({'consumed': True,  'ammunition' : 0, 'target': None, 'message': Message('Out of ammunition, weapon discarded.', tc.yellow)})

    return results
