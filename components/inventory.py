import tcod as tc

from map_objects.game_messages import Message

class Inventory:
    def __init__(self, capacity):
        self.capacity = capacity
        self.items = []

    def add_item(self, item):
        results = []

        if len(self.items) >= self.capacity:
            results.append({
                'item_added': None,
                'message': Message('You cannot carry any more, your inventory is full', tc.yellow)
            })
        else:
            results.append({
                'item_added': item,
                'message': Message('You pick up {0}!'.format(item.name), tc.light_blue)
            })

            self.items.append(item)

        return results

    def use(self, item_entity, **kwargs):
            results = []

            item_component = item_entity.item
            if item_component.use_function is None:
                results.append({'message': Message('The {0} cannot be used'.format(item_entity.name), tc.yellow)})
            else:
                if item_component.targeting and not (kwargs.get('target_x') or kwargs.get('target_y')):
                    results.append({'targeting': item_entity})
                else:
                    kwargs = {**item_component.function_kwargs, **kwargs}
                    item_use_results = item_component.use_function(self.owner, **kwargs)
                    for item_use_result in item_use_results:
                        if item_use_result.get('consumed'):
                            self.remove_item(item_entity)
                        elif item_use_result.get('ammunition') and item_use_result.get('item_used'):
                            self.modify_ammo(item_entity, item_use_result.get('ammunition'))
                    results.extend(item_use_results)

            return results

    def modify_ammo(self, item, count):
        item.item.ammunition = count

    def remove_item(self, item):
        self.items.remove(item)


    def drop_item(self, item):
        results = []

        item.x = self.owner.x
        item.y = self.owner.y

        self.remove_item(item)
        results.append({'item_dropped': item, 'message': Message('You dropped {0}'.format(item.name),
                                                                 tc.yellow)})

        return results
