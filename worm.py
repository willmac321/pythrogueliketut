from character import Character
from random import randint
import tcod as tc
from renderFunctions import RenderOrder

class Worm(Character):
    def __init__(self, icon, x, y, color, name, room, blocks = False, render_order = RenderOrder.CORPSE, fighter = None, ai = None):
        super().__init__(icon, x, y, color, name, blocks, render_order, fighter, ai)

        self.char = [icon, '<', '<']

        x2 = randint(x - 1, x + 1)
        y2 = randint(y - 1, y + 1)
        self.xArr = [x, x2, randint(x2 + 1, x2 + 1)]
        self.yArr = [y, y2, randint(y2 + 1, y2 + 1)]
        self.x = x
        self.y = y
        self.xLims = (room.x1 + 1, room.x2 - 1)
        self.yLims = (room.y1 + 1, room.y2 - 1)

        self.checkXY()

    def setXY(self, move):
        if not (move[0] == self.xArr[1] and move[1] == self.yArr[1]) and not (move[0] == self.xArr[2] and move[1] == self.yArr[2]):
            self.xArr = [move[0], self.xArr[0], self.xArr[1]]
            self.yArr = [move[1], self.yArr[0], self.yArr[1]]
            self.x = self.xArr[0]
            self.y = self.yArr[0]

    def checkXY(self):
        for i in range(len(self.xArr)):
            if not (self.xArr[i] >= self.xLims[0]):
                self.xArr[i] += 1
            elif not (self.xArr[i] <= self.xLims[1]):
                self.xArr[i] -= 1

        for i in range(len(self.yArr)):
            if not (self.yArr[i] >= self.yLims[0]):
                self.yArr[i] += 1
            elif not (self.yArr[i] <= self.yLims[1]):
                self.yArr[i] -= 1

    def moveXY(self, move):
        if not (self.xArr[0] + move[0] == self.xArr[1] and self.yArr[0] + move[1] == self.yArr[1]) and not (self.xArr[0] + move[0] == self.xArr[2] and self.yArr[0] + move[1] == self.yArr[2]):
            self.xArr = [self.xArr[0] + move[0], self.xArr[0], self.xArr[1]]
            self.yArr = [self.yArr[0] + move[1], self.yArr[0], self.yArr[1]]
            self.x = self.xArr[0]
            self.y = self.yArr[0]


#
