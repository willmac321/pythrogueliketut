#!/usr/bin/env python3
import tcod as tc
import sys
from collections import namedtuple
from components.fighter import Fighter
from components.death_functions import *
from character import *
from map_objects.gameMap import GameMap
from map_objects.game_messages import MessageLog, Message
from fov_functions import init_fov, recompute_fov
from target_circle_functions import *
from keyPress import handle_keys, handle_mouse
from renderFunctions import clear_all, render_all, RenderOrder, get_names_under_mouse, MouseStruct
from game_states import GameStates
from components.inventory import Inventory
from components.item import Item
from components.item_functions import shoot_crossbow, heal
from map_objects.menus import clear_menu
from random import randint

def main():
#----------------------------------------
#set up screen and font and color lib
#----------------------------------------
    screen_width = 80
    screen_height = 50
    map_width = 80
    map_height = 43

    colors = {
        'light_sand': (240, 231, 200),
        'light_rock' : (125, 109, 53),
        'player' : (0, 5, 5), #255, 250, 250),
        'dark_sand' : (223, 173, 69),
        'dark_rock' : (100, 51, 40),
        'light_spice' : (223, 122, 69),
        'dark_spice' : (165, 74, 41),
        'npc' : (47, 52, 133),
        'worm' : (155, 205, 222),
        'target_circle' : (159, 164, 173)
    }

    tc.console_set_custom_font('resources/arial10x10.png', tc.FONT_TYPE_GREYSCALE | tc.FONT_LAYOUT_TCOD)
    con = tc.console_init_root(screen_width, screen_height, 'Dune: The RougeLike', False)

    foreground_color = tc.white
    background_cell = tc.BKGND_NONE

#----------------------------------------
#create room variables
#----------------------------------------
    allow_room_overlap = True

    if allow_room_overlap:
        room_max_size = 20
        room_min_size = 5
        max_rooms = 35
        spice_bloom_max_size = 8
        max_spice_blooms = 6
        max_items_per_room = 7
    else:
        room_max_size = 25
        room_min_size = 10
        max_rooms = 30
        spice_bloom_max_size = 5
        max_spice_blooms = 3
        max_items_per_room = 10


#----------------------------------------
#create controls
#----------------------------------------
    key = tc.Key()
    mouse = tc.Mouse()

#----------------------------------------
#create ui and message log
#----------------------------------------

    bar_width = 20
    panel_height = 7
    panel_y = screen_height - panel_height

    message_x = bar_width + 2
    message_width = screen_width - bar_width - 2
    message_height = panel_height - 1

#console panel
    ui_panel = tc.console_new(screen_width, panel_height)

    message_log = MessageLog(message_x, message_width, message_height)

#mouseover panel
    mouse_over_panel = tc.console_new(bar_width, 2)
#----------------------------------------
#create characters
#----------------------------------------
    if allow_room_overlap:
        max_monsters_per_room = 1
    else:
        max_monsters_per_room = 3

    fighter_component = Fighter(hp = 30, defense = 2, power = 5)
    inventory_component = Inventory(26)


    player = Character('@', int(screen_width / 2), int(screen_height / 2), colors['player'], 'Player', blocks = True, render_order = RenderOrder.ACTOR, fighter = fighter_component, inventory = inventory_component)

#----------------------------------------
#put starter items in character inventory
#----------------------------------------
    item_component = Item(use_function = shoot_crossbow, targeting=True, damage=20, maximum_range=5, ammunition = randint(5, 20), radius = 5, targeting_message=Message('Left-click a target tile to shoot, or right-click to cancel.', tc.light_cyan))

    item = Character('?', int(screen_width / 2), int(screen_height / 2), tc.light_blue, 'Crossbow', render_order=RenderOrder.ITEM,item=item_component)

    player.inventory.add_item(item)


    item_component = Item(use_function = heal, amount = 4)
    item = Character('~', int(screen_width / 2), int(screen_height / 2), tc.violet, 'Spice', render_order = RenderOrder.ITEM, item = item_component)

    player.inventory.add_item(item)
    player.inventory.add_item(item)

    item_component = None
    item = None

    entities = [player]
#----------------------------------------
#setup game state
#----------------------------------------
    game_state = GameStates.PLAYERS_TURN
    previous_game_state = game_state

    targeting_item = None
#----------------------------------------
#create map
#----------------------------------------

    game_map = GameMap(map_width, map_height)
    game_map.make_map(max_rooms, room_min_size, room_max_size, map_width, map_height, player, entities, max_monsters_per_room, spice_bloom_max_size, max_spice_blooms, allow_room_overlap)

#----------------------------------------
#create fov vars and init and create target circle
#----------------------------------------

    fov_algorithm = 0
    fov_light_walls = True
    fov_radius = 10
    fov_recompute = True

    fov_map = init_fov(game_map)

    tc_recompute = False
    tc_radius = 0
    targetc_map = init_tc(game_map)
#----------------------------------------
#run loop
#----------------------------------------

    while not tc.console_is_window_closed():
        tc.sys_check_for_event(tc.EVENT_KEY_PRESS | tc.EVENT_MOUSE, key, mouse)

        if fov_recompute:
            recompute_fov(fov_map, player.x, player.y, fov_radius, fov_light_walls, fov_algorithm)
        if tc_recompute:
            recompute_tc(targetc_map, player.x, player.y, tc_radius, fov_light_walls, fov_algorithm)

        render_all(con, ui_panel, entities, game_map, fov_map, fov_recompute, targetc_map, tc_recompute, message_log, screen_width, screen_height, bar_width, panel_height, panel_y, mouse, mouse_over_panel, colors, game_state)

        tc_recompute = False
        fov_recompute = False

        tc.console_flush()

        names_under_mouse = get_names_under_mouse(mouse, entities, fov_map)

        if names_under_mouse:# and not game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY, GameStates.TARGETING):
            fov_recompute = True
            old_mouse_coords = MouseStruct(mouse.cx + 1, mouse.cy - 1, bar_width, 2)

            clear_all(con, entities, mouse_over_panel, old_mouse_coords)
        else:
            clear_all(con, entities)

        action = handle_keys(key, game_state)
        mouse_action = handle_mouse(mouse)

        move = action.get('move')
        pickup = action.get('pickup')
        show_inventory = action.get('show_inventory')
        inventory_index = action.get('inventory_index')
        exit = action.get('exit')
        fullscreen = action.get('fullscreen')
        drop_inventory = action.get('drop_inventory')

        left_click = mouse_action.get('left_click')
        right_click = mouse_action.get('right_click')

        player_turn_results = []

        if move and game_state == GameStates.PLAYERS_TURN:
            dest = (player.x + move[0], player.y + move[1])

            if not game_map.is_blocked(dest[0], dest[1]):
                target = get_blocking_entities_at_location(entities, dest[0], dest[1])
                if target:
                    attack_results = player.fighter.attack(target)
                    player_turn_results.extend(attack_results)
                else:
                    player.moveXY(move)
                    fov_recompute = True

                game_state = GameStates.ENEMY_TURN

        elif pickup and game_state == GameStates.PLAYERS_TURN:
            for e in entities:
                if e.item and e.x == player.x and e.y == player.y:
                    pickup_results = player.inventory.add_item(e)
                    player_turn_results.extend(pickup_results)
                    break
            else:
                message_log.add_message(Message('There is nothing here to pick up.', tc.yellow))

        if show_inventory:
            previous_game_state = game_state
            game_state = GameStates.SHOW_INVENTORY

        if drop_inventory:
            previous_game_state = game_state
            game_state = GameStates.DROP_INVENTORY

        if inventory_index is not None and previous_game_state != GameStates.PLAYER_DEAD and inventory_index < len(player.inventory.items):
            item  = player.inventory.items[inventory_index]

            if game_state == GameStates.SHOW_INVENTORY:
                player_turn_results.extend(player.inventory.use(item, entities=entities, fov_map=fov_map))

            elif game_state == GameStates.DROP_INVENTORY:
                player_turn_results.extend(player.inventory.drop_item(item))

        if game_state == GameStates.TARGETING:
            clear_menu(con, screen_width, screen_height)
            fov_recompute = True
            tc_recompute = True
            tc_radius = int({**targeting_item.item.function_kwargs, }.get('maximum_range'))
            if left_click:
                target_x, target_y = left_click
                item_use_results = player.inventory.use(targeting_item, ammunition = targeting_item.item.ammunition, entities=entities, fov_map=fov_map, tc_map = targetc_map, target_x=target_x, target_y=target_y)

                player_turn_results.extend(item_use_results)

            elif right_click:
                player_turn_results.append({'targeting_cancelled': True})

        if exit:
            if game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
                game_state = previous_game_state
                clear_menu(con, screen_width, screen_height)
                fov_recompute = True
            elif game_state == GameStates.TARGETING:
                clear_menu(con, screen_width, screen_height)
                fov_recompute = True
                player_turn_results.append({'targeting_cancelled': True})
            else:
                return True

        if fullscreen:
            tc.console_set_fullscreen(not tc.console_is_fullscreen())

        for player_turn_result in player_turn_results:
            message = player_turn_result.get('message')
            dead_entity = player_turn_result.get('dead')
            item_added = player_turn_result.get('item_added')
            item_consumed = player_turn_result.get('consumed')
            item_dropped = player_turn_result.get('item_dropped')
            # item_ammunition = player_turn_result.get('ammunition')
            item_used = player_turn_result.get('item_used')
            targeting = player_turn_result.get('targeting')
            targeting_cancelled = player_turn_result.get('targeting_cancelled')

            if message:
                message_log.add_message(message)

            if targeting_cancelled:
                game_state = previous_game_state

                message_log.add_message(Message('Targeting cancelled'))

            if dead_entity:
                if dead_entity == player:
                    message, game_state = kill_player(dead_entity)
                else:
                    message = kill_monster(dead_entity)

                message_log.add_message(message)

            if item_added:
                entities.remove(item_added)
                game_state = GameStates.ENEMY_TURN

            if item_consumed or item_used:
                clear_menu(con, screen_width, screen_height)
                fov_recompute = True
                game_state = GameStates.ENEMY_TURN

            if targeting:
                previous_game_state = GameStates.PLAYERS_TURN
                game_state = GameStates.TARGETING

                targeting_item = targeting

                message_log.add_message(targeting_item.item.targeting_message)

            if item_dropped:
                entities.append(item_dropped)
                clear_menu(con, screen_width, screen_height)
                fov_recompute = True
                game_state = GameStates.ENEMY_TURN

        if game_state == GameStates.ENEMY_TURN:
            for e in entities:
                if e.ai:
                    enemy_turn_results = e.ai.take_turn(player, fov_map, game_map, entities)

                    for enemy_turn_result in enemy_turn_results:
                        message = enemy_turn_result.get('message')
                        dead_entity = enemy_turn_result.get('dead')

                        if message:
                            message_log.add_message(message)

                        if dead_entity:
                            if dead_entity == player:
                                message, game_state = kill_player(dead_entity)
                            else:
                                message = kill_monster(dead_entity)

                            message_log.add_message(message)

                            if game_state == GameStates.PLAYER_DEAD:
                                break

                    if game_state == GameStates.PLAYER_DEAD:
                        break
            else:
                game_state = GameStates.PLAYERS_TURN



if __name__ == '__main__':
    main()
