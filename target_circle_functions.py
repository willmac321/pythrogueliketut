import tcod as tc

def init_tc(game_map):
    targetc_map = tc.map_new(game_map.width, game_map.height)
    for y in range(game_map.height):
        for x in range(game_map.width):
            tc.map_set_properties(targetc_map, x, y, not game_map.tiles[x][y].block_sight, not game_map.tiles[x][y].blocked)

    return targetc_map

def recompute_tc(targetc_map, x, y, radius, light_walls=True, algorithm=0):
    tc.map_compute_fov(targetc_map, x, y, radius, light_walls, algorithm)
