import tcod as tc
from collections import namedtuple
from enum import Enum, auto
from game_states import GameStates
from map_objects.menus import inventory_menu

class RenderOrder(Enum):
    CORPSE = auto()
    ITEM = auto()
    ACTOR = auto()

MouseStruct = namedtuple('MouseCoords', ['x', 'y', 'w', 'h'])

def get_names_under_mouse(mouse, entities, fov_map):
    (x, y) = (mouse.cx, mouse.cy)

    names = [e.name for e in entities if e.x == x and e.y == y and tc.map_is_in_fov(fov_map, e.x, e.y)]

    names = ', '.join(names)

    return names.capitalize()

def get_entities_under_mouse(mouse, entities, fov_map):
    (x, y) = (mouse.cx, mouse.cy)

    ent = [e for e in entities if e.x == x and e.y == y and tc.map_is_in_fov(fov_map, e.x, e.y)]

    for e in ent:
        if e.render_order == RenderOrder.ACTOR:
            return e
    return None

def create_mouse_over_panel(mouse, entities, fov_map, mouse_over_panel, bar_width):
    mouse_over_names = get_names_under_mouse(mouse, entities, fov_map)
    mouse_ent = get_entities_under_mouse(mouse, entities, fov_map)

    if mouse_over_names:
        tc.console_set_default_background(mouse_over_panel, tc.black)
        tc.console_clear(mouse_over_panel)
        tc.console_set_default_foreground(mouse_over_panel, tc.white)
        tc.console_print_ex(mouse_over_panel, 0, 0, tc.BKGND_NONE, tc.LEFT, mouse_over_names.capitalize())
        if mouse_ent:
            render_bar(mouse_over_panel, 0, 1, bar_width, 'HP', mouse_ent.fighter.hp, mouse_ent.fighter.max_hp,
                        tc.light_red, tc.darker_red)
        tc.console_blit(mouse_over_panel, 0, 0, 0, 0, 0, mouse.cx + 1, mouse.cy - 1)

def render_bar(panel, x, y, total_width, name, value, maximum, bar_color, back_color):
    bar_width = int(float(value) / maximum * total_width)

    tc.console_set_default_background(panel, back_color)
    tc.console_rect(panel, x, y, total_width, 1, False, tc.BKGND_SET)

    tc.console_set_default_background(panel, bar_color)
    if bar_width > 0:
        tc.console_rect(panel, x, y, bar_width, 1, False, tc.BKGND_SET)

    tc.console_set_default_foreground(panel, tc.white)
    tc.console_print_ex(panel, int(x + total_width / 2), y, tc.BKGND_NONE, tc.CENTER, '{0}: {1}/{2}'.format(name, value, maximum))

def render_all(con, panel, entities, game_map, fov_map, fov_recompute, targetc_map, tc_recompute, message_log,  screen_width, screen_height, bar_width, panel_height, panel_y, mouse, mouse_over_panel, colors, game_state):

    player = next((e for e in entities if e.name == 'Player'), None)

    if fov_recompute:
        #draw boundary and floor
        for y in range(game_map.height):
            for x in range(game_map.width):
                visible = tc.map_is_in_fov(fov_map, x, y)
                wall = game_map.tiles[x][y].block_sight
                spice = game_map.tiles[x][y].is_spice
                if visible:
                    if wall:
                        tc.console_set_char_background(con, x, y, colors.get('light_rock'), tc.BKGND_SET)
                    elif spice:
                        tc.console_set_char_background(con, x, y, colors.get('light_spice'), tc.BKGND_SET)
                    else:
                        tc.console_set_char_background(con, x, y, colors.get('light_sand'), tc.BKGND_SET)

                    game_map.tiles[x][y].explored = True

                elif game_map.tiles[x][y].explored:
                    if wall:
                        tc.console_set_char_background(con, x, y, colors.get('dark_rock'), tc.BKGND_SET)
                    elif spice:
                        tc.console_set_char_background(con, x, y, colors.get('dark_spice'), tc.BKGND_SET)
                    else:
                        tc.console_set_char_background(con, x, y, colors.get('dark_sand'), tc.BKGND_SET)
    if tc_recompute:
        for y in range(game_map.height):
            for x in range(game_map.width):
                target = tc.map_is_in_fov(targetc_map, x, y)
                wall = game_map.tiles[x][y].block_sight
                if target and not wall:
                    tc.console_set_char_background(con, x, y, colors.get('target_circle'), tc.BKGND_SET)
                    
    entities_in_render_order = sorted(entities, key=lambda x: x.render_order.value)

    #draw all entities
    for e in entities_in_render_order:
        draw_entity(con, e, fov_map)

    tc.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)

    create_mouse_over_panel(mouse, entities, fov_map, mouse_over_panel, bar_width)

    tc.console_set_default_background(panel, tc.black)
    tc.console_clear(panel)

    y = 1
    for m in message_log.messages:
        tc.console_set_default_foreground(panel, m.color)
        tc.console_print_ex(panel, message_log.x, y, tc.BKGND_SET, tc.LEFT, m.text)
        y += 1

    render_bar(panel, 1, 1, bar_width, 'HP', player.fighter.hp, player.fighter.max_hp,
               tc.light_red, tc.darker_red)

    tc.console_blit(panel, 0, 0, screen_width, panel_height, 0, 0, panel_y)


    if game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
        if game_state == GameStates.SHOW_INVENTORY:
            inventory_title = 'Press the key next to an item to USE it, or shft+q to cancel.\n'
        else:
            inventory_title = 'Press the key next to an item to DROP it, or shft+q to cancel.\n'

        inventory_menu(con, inventory_title, player.inventory, 50, screen_width, screen_height)


def clear_all(con, entities, mouse_over_panel = None, mouse_c = None):
    for e in entities:
        clear_entity(con, e)
    if mouse_c:
        tc.console_set_default_background(con, tc.black)
        tc.console_rect(con, mouse_c.x, mouse_c.y, mouse_c.w, mouse_c.h, False, tc.BKGND_SET)
        tc.console_print_ex(con, mouse_c.x, mouse_c.y, tc.BKGND_NONE, tc.LEFT, ' ' * mouse_c.x)
        tc.console_print_ex(con, mouse_c.x, mouse_c.y + 1, tc.BKGND_NONE, tc.LEFT, ' ' * mouse_c.x)

def draw_entity(con, e, fov_map):
    if tc.map_is_in_fov(fov_map, e.x, e.y):
        tc.console_set_default_foreground(con, e.color)
        if (e.isWorm()):
            for i in range(len(e.xArr)):
                tc.console_put_char(con, e.xArr[i], e.yArr[i], e.char[i], tc.BKGND_NONE)
        else:
            tc.console_put_char(con, e.x, e.y, e.char, tc.BKGND_NONE)

def clear_entity(con, e):
    if (e.isWorm()):
        for i in range(len(e.xArr)):
            tc.console_put_char(con, e.xArr[i], e.yArr[i], ' ', tc.BKGND_NONE)
    else:
        tc.console_put_char(con, e.x, e.y, ' ', tc.BKGND_NONE)
