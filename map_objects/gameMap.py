import tcod as tc

from random import randint
from map_objects.tile import Tile
from map_objects.rectangle import Rect
from map_objects.game_messages import Message
from character import Character
from worm import Worm
from components.ai import BasicMonster
from components.fighter import Fighter
from components.item import Item
from components.item_functions import heal, shoot_crossbow
from renderFunctions import RenderOrder

class GameMap(object):

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.tiles = self.initialize_tiles()

    def initialize_tiles(self):
        tiles = [[Tile(True) for y in range(self.height)] for x in range(self.width)]

        return tiles

    def is_blocked(self, x, y):
        if self.tiles[x][y].blocked:
            return True
        return False

    def is_spice(self, x, y):
        if self.tiles[x][y].is_spice:
            return True
        return False

    def create_spice(self, room, max_size):
        max_size = randint(1, max_size)
        count = 0
        while (count < max_size):
            if randint(1, 10) >= 3:
                x = randint(room.x1 + 1, room.x2)
                y = randint(room.y1 + 1, room.y2)
                if(self.tiles[x][y].is_spice == False):
                    self.tiles[x][y].is_spice = True
                    count += 1


    def create_room(self, room):
        for x in range(room.x1 + 1, room.x2):
            for y in range(room.y1 + 1, room.y2):
                self.tiles[x][y].blocked = False
                self.tiles[x][y].block_sight = False

    def make_map(self, max_rooms, room_min_size, room_max_size, map_width, map_height, player, entities, max_monsters_per_room, spice_bloom_max_size, max_spice_blooms, allow_room_overlap):
        rooms = []
        spice = []
        num_rooms = 0
        spice_count = 0

        for r in range(max_rooms):
            # random width and height
            w = randint(room_min_size, room_max_size)
            h = randint(room_min_size, room_max_size)
            # random position without going out of the boundaries of the map
            x = randint(0, map_width - w - 1)
            y = randint(0, map_height - h - 1)

            new_room = Rect(x, y, w, h)

            for other_room in rooms:
                if not allow_room_overlap and new_room.intersect(other_room):
                    break
            else:
                self.create_room(new_room)

                (n_x, n_y) = new_room.center()

                if num_rooms == 0:
                    #first room
                    player.x = n_x
                    player.y = n_y
                else:
                    #other rooms
                    (p_x, p_y) = rooms[num_rooms - 1].center()
                    if randint(0, 1) == 1:
                        # first move horizontally, then vertically
                        self.create_h_tunnel(p_x, n_x, p_y)
                        self.create_v_tunnel(p_y, n_y, n_x)
                    else:
                        # first move vertically, then horizontally
                        self.create_v_tunnel(p_y, n_y, p_x)
                        self.create_h_tunnel(p_x, n_x, n_y)

                if spice_count < max_spice_blooms:
                    new_spice = Rect(x, y, w, h)
                    self.create_spice(new_spice, spice_bloom_max_size)
                    spice.append(new_spice)
                    spice_count += 1

                self.place_entities(new_room, new_spice, entities, max_monsters_per_room, 2)
                # finally, append the new room to the list
                rooms.append(new_room)
                num_rooms += 1


    def create_h_tunnel(self, x1, x2, y):
        for x in range(min(x1, x2), max(x1, x2) + 1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False

    def create_v_tunnel(self, y1, y2, x):
        for y in range(min(y1, y2), max(y1, y2) + 1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False

    def place_entities(self, room, spice, entities, max_monsters_per_room, max_items_per_room):

        number_of_monsters = randint(0, max_monsters_per_room)
        number_of_items = randint(0, max_items_per_room)

        for i in range(number_of_monsters):
            x = randint(room.x1 + 1, room.x2 - 1)
            y = randint(room.y1 + 1, room.y2 - 1)

            if not any([entity for entity in entities if entity.x == x and entity.y == y]):
                if randint(0, 100) < 80:
                    fighter_component = Fighter(hp = 10, defense = 0, power = 3)
                    ai_component = BasicMonster()
                    monster = Character('h', x, y, tc.desaturated_green, 'Harkonnen', blocks = True, render_order  = RenderOrder.ACTOR, fighter = fighter_component, ai = ai_component)
                else:
                    x = randint(spice.x1 + 1, spice.x2 - 1)
                    y = randint(spice.y1 + 1, spice.y2 - 1)
                    fighter_component = Fighter(hp = 20, defense = 1, power = 5)
                    ai_component = BasicMonster()
                    monster = Worm('S', x, y, tc.darker_green, 'Worm', room, blocks = True, render_order = RenderOrder.ACTOR, fighter = fighter_component, ai = ai_component)

                entities.append(monster)

        for i in range(number_of_items):
            x = randint(spice.x1 + 1, spice.x2 - 1)
            y = randint(spice.y1 + 1, spice.y2 - 1)
            item_chance = randint(0, 100)

            if not any([entity for entity in entities if entity.x == x and entity.y == y]) and self.tiles[x][y].is_spice:
                item_component = Item(use_function = heal, amount = 4)
                item = Character('~', x, y, tc.violet, 'Spice', render_order = RenderOrder.ITEM, item = item_component)
                entities.append(item)

            x = randint(room.x1 + 1, room.x2 - 1)
            y = randint(room.y1 + 1, room.y2 - 1)
            if not any([entity for entity in entities if entity.x == x and entity.y == y]) and item_chance > 70:
                item_component = Item(use_function = shoot_crossbow, targeting=True, damage=20, maximum_range=5, ammunition = randint(5, 20), radius = 5, targeting_message=Message(
                        'Left-click a target tile to shoot, or right-click to cancel.', tc.light_cyan))
                item = Character('?', x, y, tc.light_blue, 'Crossbow', render_order=RenderOrder.ITEM,
                              item=item_component)
                entities.append(item)
