import tcod as tc


def menu(con, header, options, width, screen_width, screen_height):
    if len(options) > 26: raise ValueError('Cannot have a menu with more than 26 options.')

    # calculate total height for the header (after auto-wrap) and one line per option
    header_height = tc.console_get_height_rect(con, 0, 0, width, screen_height, header)
    height = len(options) + header_height

    # create an off-screen console that represents the menu's window
    window = tc.console_new(width, height)

    # print the header, with auto-wrap
    tc.console_set_default_foreground(window, tc.white)
    tc.console_print_rect_ex(window, 0, 0, width, height, tc.BKGND_NONE, tc.LEFT, header)

    # print all the options
    y = header_height
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ') ' + option_text
        tc.console_print_ex(window, 0, y, tc.BKGND_NONE, tc.LEFT, text)
        y += 1
        letter_index += 1

    # blit the contents of "window" to the root console
    x = int(screen_width / 2 - width / 2)
    y = int(screen_height / 2 - height / 2)
    tc.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)

def inventory_menu(con, header, inventory, inventory_width, screen_width, screen_height):
    # show a menu with each item of the inventory as an option
    if len(inventory.items) == 0:
        options = ['Inventory is empty.']
    else:
        options = [item.name if item.item.ammunition <= 0 else str(item.name + " " + str(item.item.ammunition)) for item in inventory.items]

    menu(con, header, options, inventory_width, screen_width, screen_height)

def clear_menu(con, screen_width, screen_height):
    for i in range(screen_height):
        tc.console_print_ex(con, 0, i, tc.BKGND_NONE, tc.LEFT, ' ' * screen_width)
