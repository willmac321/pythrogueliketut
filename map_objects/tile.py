
class Tile(object):

    def __init__(self, blocked, block_sight = None, spice = False, target = False):
        self.blocked = blocked

        self.is_spice = spice

        if block_sight is None:
            block_sight = blocked

        self.block_sight = block_sight

        # if block_sight:
        #     target = False
        #
        # self.target = target

        self.explored = False
