import colorsys
from collections import namedtuple
import math
import tcod as tc
from renderFunctions import RenderOrder

class Character(object):

    def __init__(self, icon, x, y, color, name, blocks = False, render_order = RenderOrder.CORPSE, fighter = None, ai = None, item = None, inventory = None):
        self.x = x
        self.y = y
        self.char = icon
        self.color = color
        self.name = name
        self.blocks = blocks
        self.ai = ai
        self.fighter = fighter
        self.render_order = render_order
        self.item = item
        self.inventory = inventory

        if self.fighter:
            self.fighter.owner = self

        if self.ai:
            self.ai.owner = self

        if self.item:
            self.item.owner = self

        if self.inventory:
            self.inventory.owner = self

    def moveXY(self, move):
        self.x += move[0]
        self.y += move[1]

    def setXY(self, newC):
        self.x = newC[0]
        self.y = newC[1]

    def isWorm(self):
        return self.name == 'Worm'

    def move_towards(self, targetCoord, game_map, entities):
        coord = [0, 0]
        coord[0] = targetCoord[0] - self.x
        coord[1] = targetCoord[1] - self.y

        dist = math.sqrt( coord[0] ** 2 + coord[1] ** 2)

        coord[0] = int(round(coord[0] / dist))
        coord[1] = int(round(coord[1] / dist))

        if not (game_map.is_blocked(self.x + coord[0], self.y + coord[1])) and not get_blocking_entities_at_location(entities, self.x + coord[0], self.y + coord[1]):
            self.moveXY(coord)

    def distance(self, x, y):
        return math.sqrt((x - self.x) ** 2 + (y - self.y) ** 2)

    def distance_to(self, other):
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt( dx ** 2 + dy ** 2)

    def move_astar(self, target, entities, game_map):
        # Create a FOV map that has the dimensions of the map
        fov = tc.map_new(game_map.width, game_map.height)

        # Scan the current map each turn and set all the walls as unwalkable
        for y1 in range(game_map.height):
            for x1 in range(game_map.width):
                tc.map_set_properties(fov, x1, y1, not game_map.tiles[x1][y1].block_sight,
                                           not game_map.tiles[x1][y1].blocked)

        # Scan all the objects to see if there are objects that must be navigated around
        # Check also that the object isn't self or the target (so that the start and the end points are free)
        # The AI class handles the situation if self is next to the target so it will not use this A* function anyway
        for entity in entities:
            if entity.blocks and entity != self and entity != target:
                if not entity.isWorm():
                    tc.map_set_properties(fov, entity.x, entity.y, True, False)
                else:
                    for i in range(len(entity.xArr)):
                        tc.map_set_properties(fov, entity.xArr[i], entity.yArr[i], True, False)

        # Set the tile as a wall so it must be navigated around
        # Allocate a A* path
        # The 1.41 is the normal diagonal cost of moving, it can be set as 0.0 if diagonal moves are prohibited
        my_path = tc.path_new_using_map(fov, 1.41)
        # Compute the path between self's coordinates and the target's coordinates
        tc.path_compute(my_path, self.x, self.y, target.x, target.y)

        # Check if the path exists, and in this case, also the path is shorter than 25 tiles
        # The path size matters if you want the monster to use alternative longer paths (for example through other rooms) if for example the player is in a corridor
        # It makes sense to keep path size relatively low to keep the monsters from running around the map if there's an alternative path really far away
        if not tc.path_is_empty(my_path) and tc.path_size(my_path) < 25:
            # Find the next coordinates in the computed full path
            x, y = tc.path_walk(my_path, True)
            if x or y:
                # Set self's coordinates to the next path tile
##todo this is causing problems with worm pathfinding
                self.setXY((x, y))

        else:
            # Keep the old move function as a backup so that if there are no paths (for example another monster blocks a corridor)
            # it will still try to move towards the player (closer to the corridor opening)
            self.move_towards((target.x, target.y), game_map, entities)

            # Delete the path to free memory
        tc.path_delete(my_path)

def get_blocking_entity(e, dest_x, dest_y):
    if e.isWorm():
        for i in range(len(e.xArr)):
            if e.blocks and e.xArr[i] == dest_x and e.yArr[i] == dest_y:
                return e
    else:
        if e.blocks and e.x == dest_x and e.y == dest_y:
            return e

def get_blocking_entities_at_location(entities, dest_x, dest_y):
    for e in entities:
        if get_blocking_entity(e, dest_x, dest_y):
            return e
    return None
