import tcod as tc
from game_states import GameStates

def handle_player_turn_keys(key):
#up
    if key.c == ord('w'):
        return {'move': (0, -1)}
#down
    elif key.c == ord('s'):
        return {'move': (0, 1)}
#right
    elif key.c == ord('d'):
        return {'move': (1, 0)}
#left
    elif key.c == ord('a'):
        return {'move': (-1, 0)}
    elif key.c == ord('q'):
        return {'move': (-1, -1)}
    elif key.c == ord('e'):
        return {'move': (1, -1)}
    elif key.c == ord('c'):
        return {'move': (1, 1)}
    elif key.c == ord('z'):
        return {'move': (-1, 1)}
    elif key.c == ord('f'):
        return {'pickup' : True}

    elif key.c == ord('r'):
        return {'show_inventory': True}

    elif key.c == ord('v'):
        return {'drop_inventory': True}
    if key.vk == tc.KEY_ENTER and key.lalt:
            return {'fullscreen' : True}
    elif key.vk == tc.KEY_ESCAPE:
        return {'exit' : True}


    return {}

def handle_targeting_keys(key):
    if key.vk == tc.KEY_ESCAPE or (key.c == ord('q') and key.shift):
        return {'exit': True}

    return {}

def handle_player_dead_keys(key):

    if key.c == ord('r'):
        return {'show_inventory': True}

    if key.vk == tc.KEY_ENTER and key.lalt:
        # Alt+Enter: toggle full screen
        return {'fullscreen': True}
    elif key.vk == tc.KEY_ESCAPE:
        # Exit the menu
        return {'exit': True}

    return {}

def handle_inventory_keys(key):

    index = key.c - ord('a')



    if key.vk == tc.KEY_ENTER and key.lalt:
        # Alt+Enter: toggle full screen
        return {'fullscreen': True}
    elif (key.c == ord('q') and key.shift) or key.vk == tc.KEY_ESCAPE:
        # Exit the menu
        return {'exit': True}
    if index >= 0:
        return {'inventory_index': index}
    return {}

def handle_mouse(mouse):
    (x, y) = (mouse.cx, mouse.cy)

    if mouse.lbutton_pressed:
        return {'left_click': (x, y)}
    elif mouse.rbutton_pressed:
        return {'right_click': (x, y)}

    return {}

def handle_keys(key, game_state):
    if game_state == GameStates.PLAYERS_TURN:
        return handle_player_turn_keys(key)
    elif game_state == GameStates.PLAYER_DEAD:
        return handle_player_dead_keys(key)
    elif game_state == GameStates.TARGETING:
        return handle_targeting_keys(key)
    elif game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
        return handle_inventory_keys(key)

    return {}
